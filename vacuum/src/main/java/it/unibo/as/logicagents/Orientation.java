package it.unibo.as.logicagents;

import java.util.Random;

/**
 * Orientation of an agent w.r.t. the absolute reference system of its environment
 */
public enum Orientation {
    NORTH("^"),
    WEST("<"),
    SOUTH("v"),
    EAST(">");

    private static Random RAND = new Random();

    public static Orientation random() {
        return Orientation.values()[RAND.nextInt(4)];
    }

    private String representation;

    Orientation(String representation) {
        this.representation = representation;
    }

    private Orientation left() {
        return Orientation.values()[(this.ordinal() + 1) % 4];
    }

    private Orientation right() {
        return Orientation.values()[(this.ordinal() + 3) % 4];
    }

    /**
     * Creates a novel {@link Orientation} by rotating the current one toward some relative {@link Direction}
     * @param direction is the {@link Direction} this {@link Orientation} should be rotated towards
     * @return a new {@link Orientation} instance
     */
    public Orientation rotate(Direction direction) {
        switch (direction) {
            case FORWARD: return this;
            case LEFT: return left();
            case RIGHT: return right();
            case BACKWARD: return back();
            default: throw new IllegalArgumentException();
        }
    }

    private Orientation back() {
        return Orientation.values()[(this.ordinal() + 2) % 4];
    }

    @Override
    public String toString() {
        return representation;
    }
}
