package it.unibo.as.logicagents;

import alice.tuprolog.Prolog;
import alice.tuprolog.exceptions.InvalidObjectIdException;
import alice.tuprolog.lib.OOLibrary;

/**
 * Base type for all artifacts
 * @param <E> the type of environments this artifact can be situated into
 */
public interface Artefact<E extends Environment> {

    /**
     * Retrieves a reference to the environment this artefact is situated into
     * @return an object of type <code>E</code>
     */
    E getEnvironment();

    /**
     * Attach this artifact to an agent
     * @param agent the agent this artifact must be attached to (it must be a {@link Prolog} instace which loaded a {@link OOLibrary} instance)
     * @param name the name of the atom this artifact should be accessible by, within <code>agent</code>'s theory
     * @throws InvalidObjectIdException in case <code>name</code> is not a valid atom
     */
    void attachTo(Prolog agent, String name) throws InvalidObjectIdException;
}
