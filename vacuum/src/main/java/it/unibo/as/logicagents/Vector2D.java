package it.unibo.as.logicagents;

import java.util.Random;

/**
 * An integer 2D position
 */
public class Vector2D {

    private static Random RAND = new Random();

    private final int x;
    private final int y;

    public static Vector2D of(int x, int y) {
        return new Vector2D(x, y);
    }

    public static Vector2D randomIn(int width, int height) {
        return new Vector2D(RAND.nextInt(width) + 1, RAND.nextInt(height) + 1);
    }

    private Vector2D(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vector2D)) return false;

        Vector2D position = (Vector2D) o;

        if (getX() != position.getX()) return false;
        return getY() == position.getY();
    }

    public Vector2D inc(int delta, Orientation orientation) {
        switch (orientation) {
            case NORTH: return Vector2D.of(x, y - delta);
            case SOUTH: return Vector2D.of(x, y + delta);
            case EAST: return Vector2D.of(x + delta, y);
            case WEST: return Vector2D.of(x - delta, y);
            default: throw new IllegalStateException();
        }
    }

    public Vector2D inc(Orientation orientation) {
        return inc(1, orientation);
    }

    @Override
    public int hashCode() {
        int result = getX();
        result = 31 * result + getY();
        return result;
    }

    @Override
    public String toString() {
        return String.format("(%d, %d)", x, y);
    }
}
