start :-
    figure_out_initial_pose,
    discover_environment_size,
    clean_environment.

figure_out_initial_pose :-
    store(current_position, (0, 0)),
    store(facing, top).

discover_environment_size :-
    todo.

clean_environment :-
    todo.

% remove this
todo :- fail.

% utility action
clean_if_needed :-
    get(current_position, (X, Y)),
    scan(dirty),
    println(["It's dirty in (", X, ", ", Y, "): sucking up"]),
    clean_up.
clean_if_needed.

% look for other utilities in Utils.pl