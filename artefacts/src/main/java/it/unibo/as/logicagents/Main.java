package it.unibo.as.logicagents;

import alice.tuprolog.Prolog;
import alice.tuprolog.Theory;
import alice.tuprolog.event.ExceptionEvent;
import alice.tuprolog.event.OutputEvent;
import alice.tuprolog.exceptions.InvalidObjectIdException;
import alice.tuprolog.exceptions.InvalidTheoryException;
import alice.tuprolog.exceptions.MalformedGoalException;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;

public class Main {
    public static void main(String[] args) throws InvalidTheoryException, MalformedGoalException, IOException, InvalidObjectIdException {
        Prolog agent = createEngineWithTheory(Main.class.getResourceAsStream("/Thermostat.pl"));
        Prolog environment = createEngineWithTheory("temp(25).");

        Artefact sensor = new TemperatureSensor(environment);
        Artefact actuator = new TemperatureActuator(environment);

        sensor.attachTo(agent, "sensor");
        actuator.attachTo(agent, "actuator");

        agent.solve("start.");
    }

    public static Prolog createEngineWithTheory(InputStream theory) throws InvalidTheoryException, IOException {
        Prolog engine = new Prolog();
        engine.setTheory(Theory.parseWithOperators(theory, engine.getOperatorManager()));
        engine.addOutputListener(Main::printOutput);
        engine.addExceptionListener(Main::printError);
        return engine;
    }

    public static Prolog createEngineWithTheory(String theory) throws InvalidTheoryException, IOException {
        return createEngineWithTheory(IOUtils.toInputStream(theory, "UTF-8"));
    }

    private static void printOutput(OutputEvent object) {
        System.out.print(cleanUpMessage(object.getMsg()));
    }

    private static void printError(ExceptionEvent object) {
        System.err.print(cleanUpMessage(object.getMsg()));
    }

    private static String cleanUpMessage(String msg) {
        if (msg.startsWith("'") && msg.endsWith("'")) {
            return msg.substring(1, msg.length() - 1);
        }
        return msg;
    }
}
